# LEARNING
# Learning about n-grams. English used as example.

###########################################################
# Example 2 - using NLTK library

import re
from nltk.util import ngrams

text = "We all change. When you think about it, we're all different people all through our lives," \
       "and that's okay, that's good, you gotta keep moving, so long as you remember all the people that you used to be.";

#value for n-grams
n = 5;

#convert to lowercases
text = text.lower();

#replace all characters which are not alphanumeric with spaces
text = re.sub(r'[^a-zA-Z0-9\text]', ' ', text);

#break utterance in the token, remove empty tokens
tokens = [token for token in text.split(" ") if token != ""];

#create n-grams
result = list(ngrams(tokens, n));

print(result)