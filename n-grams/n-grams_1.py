# LEARNING
# Learning about n-grams. English used as example.

###########################################################
# Example 1

import re

#input (text)

text = "We all change. When you think about it, we're all different people all through our lives," \
       "and that's okay, that's good, you gotta keep moving, so long as you remember all the people that you used to be.";

#function to get bi-grams
def ngrams_function(text, n):
    #table with ngrams
    ngramList = [];

    #convert to lowercases
    text = text.lower();

    #replace all characters which are not alphanumeric with spaces
    text = re.sub(r'[^a-zA-Z0-9\text]', ' ', text);

    #break utterance in the token, remove empty tokens
    tokens = [token for token in text.split(" ") if token != ""]
    print(tokens)

    #create n-grams
    for i in range(0,len(tokens)):
        ngram = " ".join(tokens[i:i+n])
        ngramList.append(ngram)

    return ngramList

print(ngrams_function(text, n=2));
